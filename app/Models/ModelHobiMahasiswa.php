<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelHobiMahasiswa extends Model
{
    use HasFactory;
    protected $table = 'mahasiswa_hobi';
    protected $fillable = [
        'id_mahasiswa', 'id_hobi'
    ];
}

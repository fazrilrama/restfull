<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelMahasiswa extends Model
{
    use HasFactory;
    protected $table = 'mahasiswa';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama', 'tgl_lahir', 'gender', 'id_jurusan'
    ];

    public function getJurusan() {
        return $this->hasOne(ModelJurusan::class, 'id', 'id_jurusan');
    }
}

<?php

namespace App\Http\Controllers;

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\ModelHobi;
use App\Models\ModelJurusan;
use App\Models\ModelMahasiswa;
use App\Models\ModelHobiMahasiswa;

class MahasiswaController extends Controller
{
    public function index() 
    {
        $mahasiswa =  ModelMahasiswa::with('getJurusan')->get();
        $response = [
            'message' => 'List Mahasiswa successfully',
            'data' => $mahasiswa
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function show($id)
    {
        $mahasiswa = ModelMahasiswa::with('getJurusan')->where('id', $id)->first();
        $response = [
            'message' => 'Detail Mahasiswa',
            'data' => $mahasiswa
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function store(Request $request) 
    {
        $validator =  Validator::make($request->all(), [
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'gender' => 'required',
            'id_jurusan' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        try {
            $mahasiswa = ModelMahasiswa::create([
                'nama' => $request->nama,
                'tgl_lahir' => $request->tgl_lahir,
                'gender' => $request->gender,
                'id_jurusan' => $request->id_jurusan,
            ]);

            $response = [
                'message' => 'Mahasiswa berhasil ditambahkan',
                'data' => $mahasiswa
            ];
            
            return response()->json($response, Response::HTTP_CREATED);
        } catch(QueryException $e) {
            return response()->json([
                'message' => 'Failed' . $e->errorInfo
            ]);
        }
    }

    public function update(Request $request, $id) 
    {
        $mahasiswa = ModelMahasiswa::where('id', $id)->first();
        $response = [
            'message' => 'Berhasil Mengupdate Data Mahasiswa',
            'data' => $mahasiswa
        ];

        if($mahasiswa == null) {
            return response()->json([
                'message' => 'Data Kosong',
            ]);
        } else {
            $mahasiswa->nama         = $request->nama;
            $mahasiswa->tgl_lahir    = $request->tgl_lahir;
            $mahasiswa->gender       = $request->gender;
            $mahasiswa->id_jurusan   = $request->id_jurusan;
            $mahasiswa->update();

            return response()->json($response, Response::HTTP_OK);
        }
    }

    public function destroy($id)
    {
        $mahasiswa = ModelMahasiswa::where('id', $id)->first();
        $null = [
            'message' => 'Data note tidak ditemukan',
        ];

        if($mahasiswa == null) {
            return response()->json($null, Response::HTTP_BAD_REQUEST);
        }

        $response = [
            'message' => 'Data note berhasil dihapus',
            'data' => $mahasiswa
        ];

        $mahasiswa->delete();

        return response()->json($response, Response::HTTP_OK);
    }
}

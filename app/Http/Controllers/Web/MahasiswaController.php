<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

use App\Models\ModelHobi;
use App\Models\ModelJurusan;
use App\Models\ModelMahasiswa;
use App\Models\ModelHobiMahasiswa;

class MahasiswaController extends Controller
{
    public function index() {
        $response = Http::get('http://restfull-laravel.test/api/mahasiswa');
        $mhs = json_decode($response->body(), true);
        $mahasiswa = $mhs['data'];

        $jurusan = ModelJurusan::get();

        // debugbar()->info($mhs);
        return view('mahasiswa.index', compact('mahasiswa', 'jurusan'));
    }

    public function edit($id) {
        $response = Http::get('http://restfull-laravel.test/api/mahasiswa/detail/' . $id);
        $mhs = json_decode($response->body(), true);
        $mahasiswa = $mhs['data'];

        $jurusan = ModelJurusan::get();
        debugbar()->info($mahasiswa);
        return view('mahasiswa.edit', compact('mahasiswa', 'jurusan'));
    }

    public function update(Request $request, $id) {
        $mahasiswa = ModelMahasiswa::where('id', $id)->first();
        
        $validator =  Validator::make($request->all(), [
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'gender' => 'required',
            'id_jurusan' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        try {
            $response = Http::put('http://restfull-laravel.test/api/mahasiswa/update/' . $id, [
                'nama' => $request->nama,
                'tgl_lahir' => $request->tgl_lahir,
                'gender' => $request->gender,
                'id_jurusan' => $request->id_jurusan,
            ]);

            $response = [
                'message' => 'Mahasiswa berhasil',
            ];
            
            return redirect('/mahasiswa')->with('message', 'Berhasil Mengupdate Data');
        } catch(QueryException $e) {
            return redirect('/mahasiswa')->with('failed', 'Gagal Mengupdate Data');
        }
    }

    public function store(Request $request) {
        $validator =  Validator::make($request->all(), [
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'gender' => 'required',
            'id_jurusan' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }

        try {
            $response = Http::post('http://restfull-laravel.test/api/mahasiswa/store', [
                'nama' => $request->nama,
                'tgl_lahir' => $request->tgl_lahir,
                'gender' => $request->gender,
                'id_jurusan' => $request->id_jurusan,
            ]);

            $response = [
                'message' => 'Mahasiswa berhasil ditambahkan',
            ];
            
            return redirect('/mahasiswa')->with('message', 'Berhasil Menambahkan Data');
        } catch(QueryException $e) {
            return redirect('/mahasiswa')->with('failed', 'Gagal Menambahkan Data');
        }
    }

    public function destroy($id) {
        $mahasiswa = ModelMahasiswa::where('id', $id)->first();
        $null = [
            'message' => 'Data note tidak ditemukan',
        ];

        if($mahasiswa == null) {
            return redirect('/mahasiswa')->with('failed', 'Gagal Menambahkan Data');
        }

        $mahasiswa->delete();
        return redirect('/mahasiswa')->with('message', 'Berhasil Menghapus Data');
    }

    public function show($id) {

        $response = Http::get('restfull-laravel.test/api/mahasiswa/detail/' . $id);
        
        $mhs = json_decode($response->body(), true);
        $mahasiswa = $mhs['data'];

        return view('mahasiswa.detail', compact('mahasiswa'));
    }
}

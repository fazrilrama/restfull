<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'mahasiswa'], function() {
    Route::get('/', [MahasiswaController::class, 'index']);
    Route::get('/detail/{id}', [MahasiswaController::class, 'show']);
    Route::post('/store', [MahasiswaController::class, 'store']);
    Route::put('/update/{id}', [MahasiswaController::class, 'update']);
    Route::delete('/destroy/{id}', [MahasiswaController::class, 'destroy']);
});
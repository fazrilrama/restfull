<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\MahasiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'mahasiswa'], function() {
    Route::get('/', [MahasiswaController::class, 'index']);
    Route::post('/store', [MahasiswaController::class, 'store']);
    Route::get('/destroy/{id}', [MahasiswaController::class, 'destroy']);
    Route::get('/show/{id}', [MahasiswaController::class, 'show']);
    Route::get('/edit/{id}', [MahasiswaController::class, 'edit']);
    Route::post('/update/{id}', [MahasiswaController::class, 'update']);
});